<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $videos = Video::all();
        return response()->json($videos);
    }

    public function create(Request $request)
    {

        function getYouTubeXMLUrl($url, $return_id_only = false)
        {

            $xml_youtube_url_base = 'https://www.youtube.com/feeds/videos.xml';
            $preg_entities        = [
                'channel_id'  => '\/channel\/(([^\/])+?)$', //match YouTube channel ID from url
                'user'        => '\/user\/(([^\/])+?)$', //match YouTube user from url
                'playlist_id' => '\/playlist\?list=(([^\/])+?)$',  //match YouTube playlist ID from url
            ];

            foreach ($preg_entities as $key => $preg_entity) {
                if (preg_match('/' . $preg_entity . '/', $url, $matches)) {
                    if (isset($matches[1])) {
                        if ($return_id_only === false) {
                            return $xml_youtube_url_base . '?' . $key . '=' . $matches[1];
                        } else {
                            return [
                                'type' => $key,
                                'id' => $matches[1],
                            ];
                        }
                    }
                }
            }
        }

        $url = $request->url;
        
        $xml_url = getYouTubeXMLUrl($url);

        $xml = simplexml_load_file($xml_url);
        $xml->registerXPathNamespace('a', 'http://www.w3.org/2005/Atom');
        $elements = $xml->xpath('//a:entry');

        foreach ($elements as $content) {
            $yt = $content->children('http://www.youtube.com/xml/schemas/2015');
            $media = $content->children('http://search.yahoo.com/mrss/');

            $video = new Video;
            $video->videoId = $yt->videoId;
            $video->videoUrl = $content->link->attributes()->href;
            $video->title = $media->group->title;
            $video->ratingCount = $media->group->community->starRating->attributes()->count;
            $video->viewCount = $media->group->community->statistics->attributes()->views;
            $video->thumbnail = $media->group->thumbnail->attributes()->url;
            $video->description = $media->group->description;

            $video->save();
        }

        return response()->json($video);
    }

    public function show($id)
    {
        $video = Video::find($id);
        return response()->json($video);
    }

    public function update(Request $request, $id)
    {
        $video = Video::find($id);

        $video->title = $request->input('title');
        $video->description = $request->input('description');
        $video->save();
        return response()->json($video);
    }

    public function destroy($id)
    {
        $video = Video::find($id);
        $video->delete();
        return response()->json('Video removed successfully.');
    }
}
