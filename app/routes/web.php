<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get(
    '/',
    function () use ($router) {
        return "Hello from swk dev team";
    }
);

$router->group(['prefix' => 'api/v1'], function () use ($router) {

    $router->get('/videos', 'VideoController@index');
    $router->post('/videos', 'VideoController@create');
    $router->get('/video/{id}', 'VideoController@show');
    $router->put('/video/{id}', 'VideoController@update');
    $router->delete('/video/{id}', 'VideoController@destroy');
});
